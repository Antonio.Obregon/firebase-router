import React from 'react';


const Navbar = ({photoURL,displayName }) => (
    <div className="navbar">
        <img height="50px" width="50px" src={photoURL} alt={photoURL}></img>
        <p>Hola {displayName}!</p>
    </div>

);

export default Navbar;