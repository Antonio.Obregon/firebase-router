import React from 'react';
import './../style.css';

const Navbar = ()=>(
    <div>
        <nav>
            <ul>
                <li>REPORTE DE PRESENTISMO</li>
                <li>REPORTE DE NUEVOS RECURSOS</li>
                <li>OTROS REPORTES</li>
                <li>MAS REPORTES</li>
            </ul>
        </nav>
    </div>
);

export default Navbar