import React, {Component} from 'react-dom';

class Hamburger extends Component {
    state = {
        active: false,
    }

    render()
    {
        return(
            <div
                className={this.state.active ? "":"active"} 
                onClick={()=>this.setState({active: !this.state.active})}>
                <span></span>
                <span></span>
                <span></span>
            </div>
        )
    }
}